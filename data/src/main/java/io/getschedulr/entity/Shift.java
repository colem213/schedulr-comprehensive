package io.getschedulr.entity;

import lombok.Data;

import java.time.Instant;

@Data
public class Shift {
  private Instant start;
  private Instant end;
}
