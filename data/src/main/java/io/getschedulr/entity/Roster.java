package io.getschedulr.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.optaplanner.core.api.domain.solution.PlanningEntityCollectionProperty;
import org.optaplanner.core.api.domain.solution.PlanningSolution;
import org.optaplanner.core.api.domain.solution.Solution;
import org.optaplanner.core.api.domain.valuerange.ValueRangeProvider;
import org.optaplanner.core.api.score.buildin.hardsoft.HardSoftScore;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

/**
 * A Roster is a list of scheduled shifts called
 * {@link io.getschedulr.ShiftAssignment ShiftAssignments}.
 *
 * @author Marc
 */
@SuppressWarnings("PMD.ShortVariable")
@Data
@EqualsAndHashCode(exclude = { "id" })
@Entity
@PlanningSolution
public class Roster implements Solution<HardSoftScore> {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  @Transient
  private List<Employee> employees;
  @Transient
  private List<Shift> shiftList;
  private List<ShiftAssignment> assignedShifts = new ArrayList<ShiftAssignment>();

  private HardSoftScore score;

  @ValueRangeProvider(id = "employees")
  public List<Employee> getEmployees() {
    return employees;
  }

  @PlanningEntityCollectionProperty
  public List<ShiftAssignment> getAssignedShifts() {
    return assignedShifts;
  }

  @Override
  public HardSoftScore getScore() {
    return score;
  }

  @Override
  public void setScore(final HardSoftScore score) {
    this.score = score;
  }

  @Override
  public Collection<? extends Object> getProblemFacts() {
    final List<Object> facts = new ArrayList<Object>();
    facts.addAll(employees);
    facts.addAll(shiftList);
    return facts;
  }

}
