package io.getschedulr.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.optaplanner.core.api.domain.entity.PlanningEntity;
import org.optaplanner.core.api.domain.variable.PlanningVariable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@SuppressWarnings("PMD.ShortVariable")
@Data
@EqualsAndHashCode(exclude = { "id" })
@Entity
@PlanningEntity
public class ShiftAssignment {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  private Shift shift;
  private Employee employee;

  @PlanningVariable(valueRangeProviderRefs = { "employees" })
  public Employee getEmployee() {
    return employee;
  }
}
