# Schedulr

Requirements
* **TEST EVERYTHING**

Scheduling Requirements
* Provide equal time to equivalent employees
* Day On Requests
* Day Off Requests
* Skill Types
* Switch Shifts
* Freeze Past Shifts
* Allow rules to be selected based on user input

Identity Requirements
* Employees should be able to create an account independently
* An employee can be employed by multiple employers
* Employees/Employers make a "friend request" to establish their relationship
* Well defined set of privileges that can be combined to create roles by users
* A default set of roles
