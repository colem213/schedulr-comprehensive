package io.getschedulr.app;

import io.getschedulr.entity.Roster;

import org.optaplanner.core.api.solver.Solver;
import org.optaplanner.core.api.solver.SolverFactory;

public final class Schedulr {

  private static final String XML_RESOURCE = "io/getschedulr/solver/rosterSolverConfig.xml";
  private static final SolverFactory<Roster> SOLVER_FACTORY =
      SolverFactory.createFromXmlResource(XML_RESOURCE);

  private static Roster problem;

  private Schedulr() {
  }

  /**
   * Main application for scheduling employees.
   */
  public static void main(final String... args) {
    final Solver<Roster> solver = SOLVER_FACTORY.buildSolver();
    problem = solver.solve(problem);
  }

  public static Roster getProblem() {
    return problem;
  }

  public static void setProblem(final Roster problem) {
    Schedulr.problem = problem;
  }
}
