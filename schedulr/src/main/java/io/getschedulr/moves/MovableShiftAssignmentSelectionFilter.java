package io.getschedulr.moves;

import io.getschedulr.entity.ShiftAssignment;

import org.optaplanner.core.impl.heuristic.selector.common.decorator.SelectionFilter;
import org.optaplanner.core.impl.score.director.ScoreDirector;

@SuppressWarnings("PMD.AtLeastOneConstructor")
public class MovableShiftAssignmentSelectionFilter implements SelectionFilter<ShiftAssignment> {
  @Override
  public boolean accept(final ScoreDirector scoreDirector, final ShiftAssignment selection) {
    return true;
  }
}
