package io.getschedulr

import static org.hamcrest.CoreMatchers.*
import static spock.util.matcher.HamcrestSupport.*

import io.getschedulr.app.Schedulr
import io.getschedulr.entity.Employee
import io.getschedulr.entity.Roster
import io.getschedulr.entity.Shift
import io.getschedulr.entity.ShiftAssignment

import spock.lang.*

import java.time.Instant
import java.time.LocalDateTime
import java.time.Month
import java.time.ZoneOffset
import java.time.temporal.ChronoUnit

class SchedulrSpec extends Specification {
  Schedulr s = new Schedulr()
  Instant now = LocalDateTime.of(2016, Month.MARCH, 26, 8, 0).toInstant(ZoneOffset.UTC)

  def "Cover Every Shift In Unscheduled Roster"() {
    given: "an employee"
    Employee employee = new Employee()
    and: "2 shifts"
    def shifts = (0..1).collect { new Shift(start: now.plus(it * 4, ChronoUnit.HOURS), end: now.plus(it * 4 + 4, ChronoUnit.HOURS)) }
    and: "an unscheduled roster"
    Roster roster = new Roster()
    roster.setEmployees([employee])
    roster.setShiftList(shifts)
    roster.setAssignedShifts(shifts.collect { new ShiftAssignment(shift: it) })
    Schedulr.setProblem(roster)

    when: "the roster is scheduled"
    s.main()

    then: "the user is assigned to each shift"
    expect s.getProblem().getAssignedShifts().collect{ it.getEmployee() }, everyItem(equalTo(employee))
  }

  def "Evenly Distribute Shifts of Equal Length in Unscheduled Roster"() {
    given: "3 employees"
    List<Employee> employees = ["Chris", "Tom", "Paul"].collect{ new Employee(name: it) }

    and: "11 shifts"
    def shifts = (0..10).collect { new Shift(start: now.plus(it * 4, ChronoUnit.HOURS), end: now.plus(it * 4 + 4, ChronoUnit.HOURS)) }

    and: "an unscheduled roster"
    Roster roster = new Roster()
    roster.setEmployees employees
    roster.setShiftList shifts
    roster.setAssignedShifts shifts.collect { new ShiftAssignment(shift: it) }
    Schedulr.setProblem roster

    when: "the roster is scheduled"
    s.main()

    then: "every shift is assigned with no employee having more than 4 or less than 3"
    def shiftsByEmployee = s.getProblem().getAssignedShifts().countBy { it.getEmployee() }
    shiftsByEmployee.max { it.value }.value == 4
    shiftsByEmployee.min { it.value }.value == 3
    expect s.getProblem().getAssignedShifts().collect { it.getEmployee() }, everyItem(notNullValue())
  }
}
